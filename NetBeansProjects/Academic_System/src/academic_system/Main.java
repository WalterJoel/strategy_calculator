package academic_system;

public class Main {
    public static void main(String[] args) {
        Sistema_Academico SA = new Sistema_Academico();
        //Patron Command
        Persona persona1 = new Profesor();
        Persona persona2 = new Alumno();
        //Cualquier orden que desea ejecutar una persona en el sistema academico POR EJEMPLO: 
        Orden orden1 = new Matricula();
        Orden orden2 = new Evaluacion(persona1);
        Orden orden3 = new Nota(); 

        //Creamos nuestra clase INVOKER que recibe nuestras ordenes e internamente
        //sabe como manejar las ordenes segun quien las ejecute.
        Invoker Inv = new Invoker(orden1,orden2,orden3);
        //Ejecutamos las ordenes 
        Inv.Matricular();
        Inv.Colocar_Nota();
        Inv.Crear_Evaluacion();   
    }    
}
